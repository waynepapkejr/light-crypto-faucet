import json
import subprocess
import sys
import configparser
import os.path

config_file = "tbch_web.config"

# Check if config file exists
if os.path.isfile(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
else:
    exit("No config file")

electron_cash_path = config["wallet"]["electron_cash_path"]
bch_wallet_file = config["wallet"]["bch_wallet_file"]
network = config["wallet"]["network"]

def supprocessing(cmd, capture_output=True, text=True):
    """Function to run the wallet commads
    It checks the network specified in the config file then adds the flag accordingly
    """
    if network == "testnet":
        cmd = cmd + ("--testnet", )
    elif network != "mainnet":
        exit("No network specified")
    cmd = ("/home/serveradmin/tbch-bot/venv/bin/python3", electron_cash_path, *cmd)
    env = os.environ.copy()
    env.update({'HOME': "/home/serveradmin/", 'USER': "serveradmin"})
    
    return subprocess.check_output(cmd, preexec_fn=demote(1000, 1000), env=env, text=text)

def demote(user_uid, user_gid):
	"""Pass the function 'set_ids' to preexec_fn, rather than just calling
	setuid and setgid. This will change the ids for that subprocess only"""

	def set_ids():
		os.setgid(user_gid)
		os.setuid(user_uid)

	return set_ids
    

# Check if daemon is connected
def check_daemon():
    """Check daemon running
    Checks if Electron Cash daemon is running
    """
    cmd = ("daemon", "status")
    print(cmd)
    daemon_status = supprocessing(cmd)
    print(daemon_status)
    try:
        json_output = json.loads(daemon_status)
        return "connected"  in json_output
    except ValueError:  # includes simplejson.decoder.JSONDecodeError
        print("daemon is not connected", file=sys.stderr)
        return False


# Doesn't support multiple loaded wallet
def check_wallet_loaded(wallet_path):
    """Check wallet loaded
    Checks if the wallet is loaded in Electron Cash daemon
    """
    cmd = ("daemon", "status")
    daemon_status = supprocessing(cmd)
    try:
        json_output = json.loads(daemon_status)
        # Check if wallet is loaded and return True or False
        return (wallet_path, True) in json_output["wallets"].items()
    except:
        print("wallet not loaded", file=sys.stderr)
        return False


def get_unused_bch_address(wallet_path):
    """Get unused BCH address"""
    unused_bch_address = supprocessing(("-w", wallet_path, "getunusedaddress"))
    return unused_bch_address


def get_unused_slp_address(wallet_path):
    """Get unused SLP address"""
    unused_slp_address = supprocessing(("-w", wallet_path, "getunusedaddress_slp"))
    return unused_slp_address

def get_bch_balance(wallet_path):
    """Get the total BCH balance in wallet"""
    bch_balance_raw = supprocessing(("-w", wallet_path, "getbalance"))
    try:
        bch_balance = json.loads(
            (bch_balance_raw).strip()
        )
        return bch_balance
    except:
        print("Decoding JSON has failed", file=sys.stderr)

def get_address_balance_bch(wallet_path, address):
    """Get the balance of a BCH address"""
    bch_address_balance_raw = supprocessing(("-w", wallet_path, "getaddressbalance", address))
    try:
        return json.loads(bch_address_balance_raw)
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def get_token_balance(wallet_path, token_id_hex):
    """Get the balance of a SLP token in wallet"""
    token_balance_raw = supprocessing(("-w", wallet_path, "getbalance_slp", token_id_hex))
    try:
        token_balance_json = json.loads(token_balance_raw)
        return token_balance_json["valid"]
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def prepare_bch_transaction(
    wallet_path, address, bch_amount
):
    """Prepare transaction
    Creates the raw transaction data
    """
    tx_data = supprocessing(("-w", wallet_path, "payto", address, bch_amount))
    try:
        tx_data_json = json.loads(tx_data)
        return tx_data_json["hex"]
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def prepare_slp_transaction(
    wallet_path, token_id_hex, address, token_amount
):
    """Prepare SLP transaction
    Creates the raw SLP transaction data
    """
    cmd = (
            "-w",
            wallet_path,
            "payto_slp",
            token_id_hex,
            address,
            token_amount
    )
    tx_data = supprocessing(cmd)
    try:
        tx_data_json = json.loads(tx_data)
    except:
        print(tx_data.stderr, file=sys.stderr)
    return tx_data_json["hex"]


def broadcast_tx(wallet_path, tx_hex):
    """Broadcast transaction
    Send the transaction to the network
    """
    broadcast = supprocessing(("-w", wallet_path, "broadcast", tx_hex))
    try:
        tx_id_json = json.loads(broadcast)
        return tx_id_json[1]
    except:
        print("Decoding JSON has failed", file=sys.stderr)


def freeze_address(wallet_path, address):
    """
    Freeze an address so it can not be used later
    """
    freeze_raw = supprocessing(("-w", wallet_path, "freeze", address))
    try:
        return json.loads(freeze_raw)
    except:
        print("Decoding JSON has failed", file=sys.stderr)
