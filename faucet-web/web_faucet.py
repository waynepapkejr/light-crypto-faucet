#!/usr/bin/env python3

import os
import sys
import logging
import configparser
import cherrypy

# Libe to allow running as deamon
from cherrypy.process.plugins import Daemonizer
# Allows continue running as a user with less privileges
from cherrypy.process.plugins import DropPrivileges
# Allow creating PID file to monitor process
from cherrypy.process.plugins import PIDFile


from decimal import Decimal

# Import from the database models
from models import save_transaction, recent_user

# Import wallet functions
from ec_slp_lib import (
    check_daemon,
    check_wallet_loaded,
    get_bch_balance,
    bch_wallet_file,
    prepare_bch_transaction,
    prepare_slp_transaction,
    broadcast_tx,
)

# Address validation using a lib from Electron Cash wallet
from validate_address import _decode_payload

from random import randint, choice

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,  # DEBUG #INFO
)

logger = logging.getLogger(__name__)


# Open configuration file
config = configparser.ConfigParser()
config.read('tbch_web.config')


bch_wallet_file = config["wallet"]["bch_wallet_file"]
token_id_hex = config["settings"]["token_id_hex"]
donate = config["settings"]["donate"]
donate_tbch = config["settings"]["donate_tbch"]
tbch_amount = config["settings"]["tbch_amount"]
token_amount = config["settings"]["token_amount"]
min_allowed_balance =  config["settings"]["min_allowed_balance"]
explorer = config["settings"]["explorer"]
slp_explorer = config["settings"]["slp_explorer"]
time_gap = config["database"].getint("time_gap")


# Check Electron Cash daemon running
daemon_running = check_daemon()
if not daemon_running:
    exit("daemon is not running") 

# check if the wallet is loaded, exit if not
wallet_loaded_bch = check_wallet_loaded(bch_wallet_file)

if not wallet_loaded_bch:
    exit("BCH wallet is not loaded") # Exit python if wallet not loaded
logger.info(f"Wallet loaded: {bch_wallet_file}")


#check wallet token balance before start, if balance lower than minimum
# limit do not start.
bch_balance = get_bch_balance(bch_wallet_file)
balance = Decimal(bch_balance["confirmed"])

if Decimal(balance) >= Decimal(min_allowed_balance):
    logger.info(f"Balance is: {balance}")
    pass
else:
    logger.info(f"No token balance: {balance}")
    # Exit python if wallet not loaded
    exit()


# Signs to be used in the sum math to increase difficulty 
plus_signs = ("➕", "+", "plus", "＋", "﹢")
random_from_tuple=choice(plus_signs)

class Form:
    """Form which the user enters his wallet address, and is shown a 
    random math captcha to solve.
    """
    def gen_random(self):
        """Generate random numbers for the math captcha
        """
        cherrypy.session['a'] = randint(0,10)
        cherrypy.session['b'] = randint(0,10)
    def index(self):
        self.gen_random()
        return (f'''
        <!DOCTYPE html>
        <html lang=en>
        <head>
            <meta charset=utf-8>
            <title>Bitcoin Cash Test Net Faucet</title>
            <link rel="icon" type="image/png" href="./favicon.ico">
        </head>
        <h1>Bitcoin Cash【  ₿ 】Test Net Faucet (tBCH)</h1>
        🚧 The Faucet is under construction 🏗️<br>
        Please 🔎 test and report any issue. 
        <h2> Faucet Form </h2>
        <form action="greet" method="GET"> Hi, what is your tbch/slp test address?<br>
        <input type="text" minlength="50" maxlength = "50" name="address" />
        <br>
        Please write the result of {cherrypy.session['a']} {choice(plus_signs)} {cherrypy.session['b']}:
        <br>
        <input type="text" maxlength = "2" name="captcha" />
        <input type="submit" value="OK" />
        </form>
        
        <h2> Info </h2>
        You can receive test tBCH and test SLP tokens<br>
        using <a href="https://github.com/simpleledger/Electron-Cash-SLP/">Electron Cash SLP wallet</a>, 
        You can also use <br> <a href="https://t.me/tbch_bot">tBCH Telegram bot</a> to get tBCH and test SLP tokens.<br><br>
        Source code: <a href="https://gitlab.com/uak/light-crypto-faucet">Light Crypto Faucet</a><br>
        For support: <a href="https://t.me/slpsell_bot">SLP bot group</a> <br>
        
        <h2> Donations </h2>
        You can donate leftover tBCH to this addess:<br>
        <code>{donate_tbch}</code><br><br>
        You can donate BCH to this address <a href="https://blockchair.com/bitcoin-cash/address/{donate}">🔗</a>:<br>
        <code>{donate}</code>
        </html>
        ''',

        """<style>  body {
                    background-color:olivedrab;
                    }
                    input[type=text] {
                    width: 400px;
                    background-color:lightgrey;
                    padding: 12px 20px;
                    margin: 8px 0;
                    box-sizing: border-box;
                    }
                    """)
        

    index.exposed=True
    
    def greet(self, address=None, captcha=None):
        """Function to check the verify captcha and verify entered address
        
        """
        try:
            result = cherrypy.session['a'] + cherrypy.session['b']
        except:
            return("no results error")
        
        # Verify address format and captcha
        if len(address) == 50 and _decode_payload(address) and (int(captcha) == result):
            recipient_address = address
            global tx_id

            if address.startswith("bchtest"):
                coin_type = "tbch"
                # Check if user have requested tokens before, checks against
                # session, ip and address
                if not recent_user(cherrypy.session._id, cherrypy.request.remote.ip, recipient_address, coin_type):
                    #print("cherrypy.session._id 2",cherrypy.session._id)
                #global tx_id
                    amount = tbch_amount
                    tx_hex = prepare_bch_transaction(
                    bch_wallet_file,
                    recipient_address,
                    amount,
                    )
                    
                    # Brodacast transaction using EC SLP lib
                    
                    tx_id = broadcast_tx(bch_wallet_file, tx_hex)
                    transaction_status = 1
                    # Reset the captcha so same result number can't be used
                    self.gen_random()
                    logger.info(f"Transaction id is {tx_id}")
                    # save transaction to the database
                    save_transaction(
                        cherrypy.session._id,
                        cherrypy.request.remote.ip,
                        recipient_address,
                        coin_type,
                        amount,
                        transaction_status,
                        tx_id
                        )
                    return f"<h1> your transaction id <a href=\"{explorer}{tx_id}\">{tx_id}</a> <br> I sent you: {amount} {coin_type}"
                else:
                    return f"You can not withdrown now, try after 12 hours"
            elif address.startswith("slptest"):
                coin_type = "sTST"
                # Check if user have requested tokens before, checks against
                # session, ip and address
                if not recent_user(cherrypy.session._id, cherrypy.request.remote.ip, recipient_address, coin_type):
                    amount = token_amount
                    # Prepare transaction using EC SLP lib
                    tx_hex = prepare_slp_transaction(
                        bch_wallet_file,
                        token_id_hex,
                        recipient_address,
                        amount,
                    )
                    
                    # Brodacast transaction using EC SLP lib
                    tx_id = broadcast_tx(bch_wallet_file, tx_hex)
                    self.gen_random()
                    transaction_status = 1
                    logger.info(f"Transaction id is {tx_id}")
                    save_transaction(
                        cherrypy.session._id,
                        cherrypy.request.remote.ip,
                        recipient_address,
                        coin_type,
                        amount,
                        transaction_status,
                        tx_id
                        )
                    return f"<h1> your transaction id <a href=\"{explorer}{tx_id}\">{tx_id}</a> <br> I sent you: {amount} {coin_type}"
                else:
                    return f"You can not withdrown now, try after 12 hours"
            else: 
                return """address prefix is not known"""
                logger.info("address prefix is not known")

            print("this got excuted")
        else:
            return """Adderss is invalid or captcha is wrong"""
    
    greet.exposed=True

if __name__ == '__main__':


  #d = Daemonizer(cherrypy.engine)
  #d.subscribe()
  
  # Drop privileges from root to a different user
  # root is required for deamon running and reading SSL certificates
  DropPrivileges(cherrypy.engine, uid=1000, gid=1000).subscribe()
  PIDFile(cherrypy.engine, '/var/run/myapp.pid').subscribe()

  
  cherrypy.quickstart(Form(),config="config.conf")

