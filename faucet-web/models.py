from peewee import *
from peewee import IntegrityError
from datetime import datetime, timedelta
import configparser

config = configparser.ConfigParser()
config.read('tbch_web.config')

database_file = config["database"]["database_file"]
time_gap = int(config["database"]["time_gap"])

db = SqliteDatabase(database_file)

class Transactions(Model):
    """The database class"""
    _id = AutoField(unique=True)
    session_id = CharField(max_length=60)
    ip = CharField(max_length=30)
    user_address = CharField(max_length=60)
    coin_type = CharField(max_length=10)
    amount_sent = DecimalField()
    success = BooleanField()
    tx_id = CharField(max_length=60)
    date = TimestampField()
	

    class Meta:
        database = db # This model uses the "db" database.

Transactions.create_table()

def save_transaction(session_id, ip, user_address, coin_type, amount, transaction_status, tx_id):
    """ Checks if a Telegram user is present in the database.
    Returns True if a user is created, False otherwise.
    """
    db.connect(reuse_if_open=True)
    try:
        Transactions.create(
            session_id=session_id,
            ip=ip,
            user_address = user_address,
            coin_type = coin_type,
            amount_sent = amount,
            date = datetime.now(),
            success = transaction_status,
            tx_id = tx_id
        )
        db.close()
        return True
    except IntegrityError:
        db.close()
        return False

def recent_user(session_id, ip, user_address, coin_type):
    """check if the user have already requested the coin before 
    
    """
    db.connect(reuse_if_open=True)
    try:
        print("Transactions.session_id", Transactions.session_id)
        # intervals allowed between requests in hours
        cutoff = datetime.now() - timedelta(hours=time_gap)
        # Check if in the sepcified period, the user have requested coins
        # more than one time, based on their session ID, IP address, coin address
        query = Transactions.select().where((Transactions.date >= cutoff) & (Transactions.coin_type == coin_type) & ((Transactions.session_id == session_id) | (Transactions.ip == ip) | (Transactions.user_address == user_address)))
        if query.exists():
            return True
        else:
            return False
    except IntegrityError:
        db.close()
        return False

