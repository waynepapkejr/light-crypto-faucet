# Modifed from Electron Cash Wallet address verification 
# https://github.com/Electron-Cash/Electron-Cash/blob/master/electroncash/cashaddr.py

_CHARSET = "qpzry9x8gf2tvdw0s3jn54khce6mua7l"

def _polymod(values):
    """Internal function that computes the cashaddr checksum."""
    c = 1
    for d in values:
        c0 = c >> 35
        c = ((c & 0x07ffffffff) << 5) ^ d
        if (c0 & 0x01):
            c ^= 0x98f2bc8e61
        if (c0 & 0x02):
            c ^= 0x79b76d99e2
        if (c0 & 0x04):
            c ^= 0xf33e5fb3c4
        if (c0 & 0x08):
            c ^= 0xae2eabe2a8
        if (c0 & 0x10):
            c ^= 0x1e4f43e470
    retval= c ^ 1
    return retval

def _prefix_expand(prefix):
    """Expand the prefix into values for checksum computation."""
    retval = bytearray(ord(x) & 0x1f for x in prefix)
    # Append null separator
    retval.append(0)
    return retval

def _decode_payload(addr):
    """Validate a cashaddr string.
    Throws CashAddr.Error if it is invalid, otherwise returns the
    triple
    """
    lower = addr.lower()
    if lower != addr and addr.upper() != addr:
        raise ValueError('mixed case in address: {}'.format(addr))

    parts = lower.split(':', 1)
    if len(parts) != 2:
        raise ValueError("address missing ':' separator: {}".format(addr))

    prefix, payload = parts
    if not prefix:
        raise ValueError('address prefix is missing: {}'.format(addr))
    if not all(33 <= ord(x) <= 126 for x in prefix):
        raise ValueError('invalid address prefix: {}'.format(prefix))
    if not (8 <= len(payload) <= 124):
        raise ValueError('address payload has invalid length: {}'
                         .format(len(addr)))
    try:
        data = bytes(_CHARSET.find(x) for x in payload)
    except ValueError:
        raise ValueError('invalid characters in address: {}'
                            .format(payload))

    if _polymod(_prefix_expand(prefix) + data):
        raise ValueError('invalid checksum in address: {}'.format(addr))

    if lower != addr:
        prefix = prefix.upper()

    return True

